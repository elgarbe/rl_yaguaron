%Ellipsoid Fitting - Least Square

%clear all;
%close all;
% home;

clear all;
close all;
clc;

%%
bag1=rosbag('/mnt/exchng_ntfs/01_DOCTORADO/08_ASV/CalibracionMagnetometro/calibracion_mag_3D.bag');
bsel1 = select(bag1,'Topic','/chori/imu/mag_raw');
msgStructs1 = (readMessages(bsel1,'DataFormat','struct'));

mx = cellfun(@(m) m.Vector.X, msgStructs1);
my = cellfun(@(m) m.Vector.Y, msgStructs1);
mz = cellfun(@(m) m.Vector.Z, msgStructs1);
%% Initial values
% a=4;
% b=2;
% c=1;
% 
% ang_1=45*pi/180;
% ang_2=45*pi/180;
% ang_3=45*pi/180;
% 
% ho=[2;2;2];
% 
data_number_per_ring=200;
ring_number=8;
% 
% sigma=0.001;
% 
% %% 
%    
% h=zeros(3,data_number_per_ring*(ring_number-1));
% 
% tita=linspace(0,pi,ring_number);
% phi=linspace(0,2*pi,data_number_per_ring);
% 
% rot_matrix=angle2dcm(ang_1,ang_2,ang_3);
% 
% mu = [0 0 0];
% Sigma = [sigma 0 0; 0 sigma 0; 0 0 sigma]; 
% R = chol(Sigma);
% 
% for j=1:ring_number-1
%     noise = repmat(mu,data_number_per_ring,1) + randn(data_number_per_ring,3)*R;
%     noise=noise';
%     
%     for i=1:data_number_per_ring
%         h(:,i+(j-1)*data_number_per_ring)=rot_matrix*([a*cos(tita(j))*sin(phi(i)); b*sin(tita(j))*sin(phi(i)); c*cos(phi(i))])+noise(:,i)+ho; 
%     end
% end
% 
% h=[mx,my,mz]';
% mag_data=h';
%%
h=[mx,my,mz]';
mag_data = [mx, my, mz];
n=size(mag_data,1);

x=mag_data(:,1);
y=mag_data(:,2);
z=mag_data(:,3);

 S = [ x.*x, y.*y, z.*z, 2*x.*y, 2*x.*z, 2*y.*z, x, y, z ];
 
 w= (S'*S)\(S'*ones(n,1));
 
 A=[w(1) w(4) w(5);
    w(4) w(2) w(6);
    w(5) w(6) w(3)];

 b=[w(7);
    w(8);
    w(9)];

center=-0.5*(A\b)

M=A/(0.25*(b'*(A\b)) + 1);

M_firm = sqrt(M)

[Q D]=eig(M);

[ evecs evals ] = eig( M);
radii = sqrt( 1 ./ diag( evals ) );

h_corr=(M^(1/2)*([x(:), y(:), z(:)]' -repmat(center,1,n)));

a=radii(1);
b=radii(2);
c=radii(3);

rot_matrix=Q;   
h_ccc=zeros(3,data_number_per_ring*(ring_number-1));

tita=linspace(0,pi,ring_number);
phi=linspace(0,2*pi,data_number_per_ring);

for j=1:ring_number-1
      
    for i=1:data_number_per_ring
        h_ccc(:,i+(j-1)*data_number_per_ring)=rot_matrix*([a*cos(tita(j))*sin(phi(i)); b*sin(tita(j))*sin(phi(i)); c*cos(phi(i))])+center; 
    end
end

norm_corr=zeros(1,n);
norm_or=zeros(1,n);
for i=1:n
    norm_corr(i)=norm(h_corr(:,i));
    norm_or(i)=norm(h(:,i)-center);
end

figure
%%%%%%%%%%%%%%%%%%%%Dibujo%%%%%%%%%%%%%%%%%%%%%%%%
plot3(h_ccc(1,:),h_ccc(2,:),h_ccc(3,:),'-r','LineWidth',2);
hold on;
plot3(h(1,:),h(2,:),h(3,:),'.');
grid on;
hold off;
view(65, 26)

title('Ajuste de mediciones del vector magnetico','fontweight','b');
%text(65,1,sprintf('Longitud=%0.5g \nPaso=%0.5g \nFase=%0.5g°',p,mu,fase),'fontweight','b');
%axis([0 90 0 1.5]);
%h = legend('X','Y','Z','Location','NorthWest');
%set(h,'Interpreter','none');
%hold off
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%print -f1 -r600 -dpdf '/home/federico/Dropbox/Tesis/DibujosMatlabInforme/Mag/mag_sim_ellipsoid.pdf'
%system('pdfcrop /home/federico/Dropbox/Tesis/DibujosMatlabInforme/Mag/mag_sim_ellipsoid.pdf /home/federico/Dropbox/Tesis/DibujosMatlabInforme/Mag/mag_sim_ellipsoid.pdf > /dev/null');

%%%%%%%%%%%%%%%%%%%%Dibujo%%%%%%%%%%%%%%%%%%%%%%%%
figure
plot(norm_corr(1:n),'-b','LineWidth',1);
hold on;
plot(norm_or(1:n),'-r','LineWidth',1);
hold off;
grid on;
xlim([1,n])
title('Norma del vector magnetico corregido','fontweight','b');
%text(65,1,sprintf('Longitud=%0.5g \nPaso=%0.5g \nFase=%0.5g°',p,mu,fase),'fontweight','b');
%axis([0 90 0 1.5]);
%h = legend('X','Y','Z','Location','NorthWest');
%set(h,'Interpreter','none');
%hold off
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%print -f2 -r600 -dpdf '/home/federico/Dropbox/Tesis/DibujosMatlabInforme/Mag/mag_sim_norm.pdf'
%system('pdfcrop /home/federico/Dropbox/Tesis/DibujosMatlabInforme/Mag/mag_sim_norm.pdf /home/federico/Dropbox/Tesis/DibujosMatlabInforme/Mag/mag_sim_norm.pdf > /dev/null');
