#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 30 18:52:30 2018 -0300

@author: elgarbe
"""
# NumPy
import numpy as np
# ROS Python API
import rospy
# Import the messages we're interested in sending and receiving, and having and sharing
from nav_msgs.msg import Odometry

MSG_QUEUE_MAXLEN = 10

class RobotAdapterNode(object):
    """Este nodo es para un robot.

    """
    def __init__(self):
        # global_position = rospy.get_param('~from/position/global', 'mavros/global_position/global')
        odom_local = rospy.get_param('~from/position/local', 'asv/mavros/global_position/local')
        pose_local_topic = rospy.get_param('~to/position/local', 'asv/odom')

        self.odom0 = Odometry()
        # Tomo una primera posicion
        rospy.loginfo("[ODOM_GLOBAL] Waiting for position initialization..")
        self.odom0 = rospy.wait_for_message(odom_local, Odometry)
        rospy.loginfo('[ODOM_GLOBAL] Odometría inicial del sistema: %s', self.odom0.pose.pose)

        # Registro el publisher
        self.local_publisher = rospy.Publisher(
            pose_local_topic,
            Odometry,
            queue_size=MSG_QUEUE_MAXLEN)
        rospy.loginfo('[ODOM_GLOBAL] Will publish vehicle odometry (relative to starting position) to topic: %s', pose_local_topic)

        # Me suscribo al tópico donde se envian los datos de odometría
        self.odom_subs = rospy.Subscriber(
            odom_local,
            Odometry,
            self.handle_odometry)
        rospy.loginfo('[ODOM_GLOBAL] Subscribing to odometry topic: %s', odom_local)


    def handle_odometry(self, msg):
        # Creo el mensaje con los datos necesarios
        message = msg
        message.header.frame_id = "odom"
        message.child_frame_id = "fcu"
        message.pose.pose.position.x = msg.pose.pose.position.x - self.odom0.pose.pose.position.x
        message.pose.pose.position.y = msg.pose.pose.position.y - self.odom0.pose.pose.position.y
        message.pose.pose.position.z = msg.pose.pose.position.z - self.odom0.pose.pose.position.z

        # Publico el mensaje
        self.local_publisher.publish(message)


    def shutdown(self):
        """Unregisters publishers and subscribers and shutdowns timers"""
        self.local_publisher.unregister()
        self.odom_subs.unregister()
        rospy.loginfo("[ODOM_GLOBAL] Sayonara ODOM_GLOBAL. Nos vemo' en Disney.")


def main():
    """Entrypoint del nodo"""
    rospy.init_node('odom_global', anonymous=True, log_level=rospy.INFO)
    node = RobotAdapterNode()

    try:
        rospy.spin()
    except KeyboardInterrupt:
        rospy.loginfo("[ODOM_GLOBAL] Received Keyboard Interrupt (^C). Shutting down.")

    node.shutdown()

if __name__ == '__main__':
    main()
