#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import rospy
from sensor_msgs.msg import Imu

# Constants that may change in multiple places
MSG_QUEUE_MAXLEN = 50

class RepubImuCov:

    def __init__(self):

        self.imu_sub = rospy.Subscriber("/imu/data", Imu, self.imu_cb)
        # Me suscribo al tópico donde se envian los datos de odometría
        self.imu_pub   = rospy.Publisher("/imu/data_cov", Imu, queue_size=10)


    def imu_cb(self, dato):
        imu_msg = dato
        imu_msg.orientation.x = orient[0]
        imu_msg.orientation.y = orient[1]
        imu_msg.orientation.z = orient[2]
        imu_msg.orientation.w = orient[3]

        self.imu_pub.publish(imu_msg)
        # rospy.loginfo("Heading from Imu: %05.3f", heading)



def main(args):
    ic = RepubImuCov()
    rospy.init_node('repub_imu_cov', anonymous=True)
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print("Shutting down")

if __name__ == '__main__':
    main(sys.argv)
